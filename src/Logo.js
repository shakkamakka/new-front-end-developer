export default function Logo(props) {
  return (
    <div className="logo">
      <img
        {...props}
        alt="Pokémon"
        src={process.env.PUBLIC_URL + '/logo.png'}
      />
    </div>
  );
}
