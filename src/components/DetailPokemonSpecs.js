
const DetailPokemonSpecs = ({ data }) => {
  return (
    <div className="pokemonspecs">
      <div>
        <h2>Stats:</h2>
        <div className="stats">
          {data.Pokemon.stats.map((stat, index) => (
            <div key={index}>
              <span className="name">{stat.name}</span>
              <span className="value">{stat.value}</span>
              {(index === 1 || index === 3) && <br />}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DetailPokemonSpecs;
