
const StatImage = ({pokedata}) => {
    return (
        <div className="statimage">
            <img src={pokedata.Pokemon.image} alt={pokedata.Pokemon.name} />
            <h1>{pokedata.Pokemon.name}</h1>
        </div>
    )
}
StatImage.defaultProps={
    pokedata:[]
}

export default StatImage
