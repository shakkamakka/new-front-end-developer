import { TypeColors } from '../data/TypeColors'

const StatTypes = ({pokedata}) => {
    return (
        <div className="stattypes">
            {pokedata.Pokemon.types.map((stat, index) => {
                const backgroundColor = TypeColors[stat.name];
                return(
                <span key={index} style={{backgroundColor}}>{stat.name}</span>)})}
        </div>
    )
}

export default StatTypes
