import React from "react";
import { useQuery } from "graphql-hooks";
import DetailPokemonSpecs from "./DetailPokemonSpecs";
import DetailPokemonMoves from "./DetailPokemonMoves";
import PropTypes from "prop-types";
import StatImage from "./StatImage";
import StatTypes from "./StatTypes";

const StatContainer = ({ name }) => {
  const GET_POKEMON_SPECS = `query Search($name: String!) {
  Pokemon(name: $name) {
    id
    name
    image
    abilities {
      name
    }
    stats {
      name
      value
    }
    moves{
      name
      type
      learnMethod
    }
    types {
      name
    }
  }
}`;
  const { loading, error, data } = useQuery(GET_POKEMON_SPECS, {
    variables: {
      name: name,
    },
  });

  if (loading) return "Loading...";
  if (error) return "Name not found";

  return (
    <div className="statcontainer">
      <StatImage pokedata={data} />
      <StatTypes pokedata={data} />
      <DetailPokemonSpecs data={data} />
      <DetailPokemonMoves data={data} />
    </div>
  );
};

StatContainer.defaultProps = {
  name: "pikachu",
};
StatContainer.propTypes = {
  name: PropTypes.string,
};
export default StatContainer;
