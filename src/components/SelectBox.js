import React from "react";
import { useState } from "react";
import SelectBoxList from "./SelectBoxList";

// Filter Pokemons
const SelectBox = ({ onSelect }) => {
  const [pokemonFilter, setPokemonFilter] = useState("");

  return (
    <div className="selectbox">
      <h2>Select a Pokemon</h2>
      <input
        type="text"
        value={pokemonFilter}
        onChange={(e) => setPokemonFilter(e.target.value)}
        placeholder="Filter"
      />
      <br />
      <SelectBoxList pokemonfilter={pokemonFilter} onSelect={onSelect} />
    </div>
  );
};

export default SelectBox;
