import React from "react";
import { useState } from "react";

const DetailPokemonMoves = ({ data }) => {
  const [moves, setMoves] = useState([]);
  const uniqueLearnMethod = [
    ...new Set(data.Pokemon.moves.map((move) => move.learnMethod)),
  ];
  const [learnMethod, setLearnMethod] = useState("");
  const learnMethodList = data.Pokemon.moves.filter(
    (o) => o.learnMethod === learnMethod
  );


  const setRightMove = (learnmethod, name) => {
    // check if learnmethod already exists in selected moves, otherwise replace it
    const index = moves.findIndex(m=>m.method === learnmethod);
    const editMoves = [...moves];
    if (index>-1) editMoves[index].name = name;
    // update state if the learnmethod doesn't exist yet
    const updateMoves = [
      ...moves,
      {
        id: moves.length + 1,
        method: learnmethod,
        name
      }
    ];
    setMoves( index===-1 ? updateMoves : editMoves); 
  };

  return (
    <div className="moves">
      <div>
        <h2>Selected Moves:</h2>
        <table>

        {moves.map((move, index)=>(
          <div key={index} className="inline">
            <span className="method">{move.method}</span>
            <span className="methodmove">{move.name}</span>
          </div>
        ))}
        </table>
      </div>
      <div>
        <h2>
          Select a learnmethod: <br />
          {uniqueLearnMethod.map((learnmethod, index) => (
              <span
                key={index}
                onClick={() => setLearnMethod(learnmethod)}
                className={
                  learnMethod === learnmethod
                    ? "learnmethodselected"
                    : "learnmethods"
                }
              >
                {learnmethod}
              </span>
          ))}
        </h2>
        <div className="moves">
          {learnMethodList.map((o, i) => (
            <span key={i} onClick={() => setRightMove(learnMethod, o.name)}>
              {o.name} <br />
            </span>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DetailPokemonMoves;
